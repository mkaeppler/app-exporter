package testhelpers

import (
	"path"
	"runtime"
)

var (
	_, thisfile, _, _ = runtime.Caller(0)
	TestDir           = path.Join(path.Dir(thisfile), "../..", "test")
)

func FixtureFilePath(dataPath string) string {
	return path.Join(TestDir, "data", dataPath)
}
