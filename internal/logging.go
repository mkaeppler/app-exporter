package internal

import (
	"log"
	"os"
)

var debugLog = os.Getenv("DEBUG") != ""

func Log(el ...interface{}) {
	if debugLog {
		log.Default().Println(el...)
	}
}
