package render

import (
	"bufio"
	"io"
	"strconv"

	"gitlab.com/gitlab-org/app-exporter/internal/probe"
	"gitlab.com/gitlab-org/app-exporter/internal/probe/mmapdb"
)

func TextAsync(ch <-chan probe.ProbeResult, w io.Writer) error {
	bw := bufio.NewWriterSize(w, 32*1024)
	defer bw.Flush()

	observedMetrics := make(map[string]struct{}, 100)

	for res := range ch {
		if res.Error != nil {
			return res.Error
		}
		appendMetricGroup(bw, &res.MGroup, observedMetrics)
	}

	return nil
}

/**
	See https://prometheus.io/docs/instrumenting/exposition_formats/#text-based-format

		metric_name [
  		"{" label_name "=" `"` label_value `"` { "," label_name "=" `"` label_value `"` } [ "," ] "}"
		] value [ timestamp ]
*/
func Text(mgroups []probe.MetricGroup, w io.Writer) error {
	ch := make(chan probe.ProbeResult)
	go func() {
		for _, mgroup := range mgroups {
			ch <- probe.ProbeResult{MGroup: mgroup}
		}
		close(ch)
	}()

	return TextAsync(ch, w)
}

func appendMetricGroup(w io.Writer, mg *probe.MetricGroup, observedMetrics map[string]struct{}) {
	sb := make([]byte, 0, 4096)

	var decoded mmapdb.DecodedKey

	for _, sample := range mg.Samples {
		err := mmapdb.Decode(&decoded, sample, mg.MetricGroupMetadata)
		if err != nil {
			continue
		}

		if _, ok := observedMetrics[decoded.Metric]; !ok {
			sb = sb[:0]
			sb = append(sb, "# HELP "...)
			sb = append(sb, decoded.Metric...)
			sb = append(sb, " Multiprocess metric\n"...)

			sb = append(sb, "# TYPE "...)
			sb = append(sb, decoded.Metric...)
			sb = append(sb, " "...)
			sb = append(sb, mg.MetricGroupMetadata.Type...)
			sb = append(sb, "\n"...)
			w.Write(sb)

			observedMetrics[decoded.Metric] = struct{}{}
		}

		sb = sb[:0]
		sb = append(sb, decoded.Name...)
		writeLabels(&sb, decoded.Labels)
		sb = append(sb, " "...)
		sb = strconv.AppendFloat(sb, sample.Value, 'f', -1, 64)
		sb = append(sb, "\n"...)
		w.Write(sb)
	}
}

// From golang: https://cs.opensource.google/go/go/+/refs/tags/go1.17.6:src/sort/sort.go;drc=refs%2Ftags%2Fgo1.17.6;l=34
// To not allocate memory
func insertionSort(data probe.LabelSlice, a, b int) {
	for i := a + 1; i < b; i++ {
		for j := i; j > a && data.Less(j, j-1); j-- {
			data.Swap(j, j-1)
		}
	}
}

func writeLabels(sb *[]byte, labels probe.LabelSlice) {
	if len(labels) == 0 {
		return
	}

	insertionSort(labels, 0, len(labels))

	*sb = append(*sb, "{"...)
	for i, lv := range labels {
		if i > 0 {
			*sb = append(*sb, "\","...)
		}
		*sb = append(*sb, lv.Name...)
		*sb = append(*sb, "=\""...)
		*sb = append(*sb, lv.Value...)
	}
	*sb = append(*sb, "\"}"...)
}
