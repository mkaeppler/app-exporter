package render

import (
	"bytes"

	"gitlab.com/gitlab-org/app-exporter/internal/probe/mmapdb"
)

func MmapFilesText(dir string, glob string) (string, error) {
	mgroups, err := mmapdb.NewProbe(dir, glob).Probe()
	if err != nil {
		return "", err
	}

	var buf bytes.Buffer
	err = Text(mgroups, &buf)
	return buf.String(), err
}
