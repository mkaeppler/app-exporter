package probe

import "strings"

type Label struct {
	Name  string
	Value string
}

type Sample struct {
	Key   string // A unique identifier for this sample. Used for merging samples.
	Value float64
}

type MetricGroupMetadata struct {
	Type string
	Agg  string
	Pid  string
}

type MetricGroup struct {
	MetricGroupMetadata
	Samples SampleSlice
}

type ProbeResult struct {
	MGroup MetricGroup
	Error  error
}

type LabelSlice []Label
type SampleSlice []Sample

type Probe interface {
	Probe() ([]MetricGroup, error)
	ProbeAsync(ch chan<- ProbeResult) error
}

func (ss SampleSlice) Len() int {
	return len(ss)
}

func (ss SampleSlice) Swap(a, b int) {
	ss[a], ss[b] = ss[b], ss[a]
}

// TODO: the fact that this produces a sensible order is an implementation detail
// to mmap, and might not work with other probes.
func (ss SampleSlice) Less(a, b int) bool {
	return ss[a].Key < ss[b].Key
}

func (ls LabelSlice) Len() int {
	return len(ls)
}

func (ls LabelSlice) Swap(a, b int) {
	ls[a], ls[b] = ls[b], ls[a]
}

func (ls LabelSlice) Less(a, b int) bool {
	v1 := ls[a]
	v2 := ls[b]
	cmp := strings.Compare(v1.Name, v2.Name)

	if cmp < 0 {
		return true
	} else if cmp > 0 {
		return false
	} else {
		return v1.Value < v2.Value
	}
}
