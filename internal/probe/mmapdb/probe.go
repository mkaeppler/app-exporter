package mmapdb

import (
	"fmt"
	"io/fs"
	"os"
	"path"
	"sort"
	"sync"

	"gitlab.com/gitlab-org/app-exporter/internal/probe"
)

type mmapdbProbe struct {
	path string
	glob string
}

type metricGroupKey struct {
	typ string
	agg string
}

type visitedSampleMap map[string]float64

func NewDefaultProbe(mmapDir string) probe.Probe {
	return &mmapdbProbe{mmapDir, "*.db"}
}

func NewProbe(mmapDir string, glob string) probe.Probe {
	return &mmapdbProbe{mmapDir, glob}
}

func (p *mmapdbProbe) ProbeAsync(ch chan<- probe.ProbeResult) error {
	fileSystem := os.DirFS(p.path)
	files, err := fs.Glob(fileSystem, p.glob)
	if err != nil {
		return err
	}

	fileGroups, err := groupFiles(files)
	if err != nil {
		return err
	}

	// Map and parse each group of paths to a MetricGroup.
	wg := sync.WaitGroup{}
	for key := range fileGroups {
		key_ := key
		wg.Add(1)
		go func() {
			defer wg.Done()

			p.processMetricGroup(ch, key_, fileGroups[key_])
		}()
	}

	wg.Wait()

	return nil
}

func (p *mmapdbProbe) processMetricGroup(ch chan<- probe.ProbeResult, key metricGroupKey, filenames []string) {
	var visitedSamples visitedSampleMap

	metricsCh := make(chan *MetricFile, 1) // allow to buffer a few files

	go func() {
		for _, f := range filenames {
			filePath := path.Join(p.path, f)

			metricFile, err := ParseFile(filePath)
			if err != nil {
				ch <- probe.ProbeResult{Error: fmt.Errorf("Failed processing %s: %w", filePath, err)}
				return
			}

			metricsCh <- metricFile
		}

		close(metricsCh)
	}()

	for metricFile := range metricsCh {
		if metricFile.Aggregation == AGGREGATE_ALL && metricFile.MetricType == METRIC_TYPE_GAUGE {
			mgroup := probe.MetricGroup{
				MetricGroupMetadata: probe.MetricGroupMetadata{
					Type: metricFile.MetricType,
					Agg:  metricFile.Aggregation,
					Pid:  metricFile.Pid,
				},
				Samples: metricFile.Samples,
			}
			ch <- probe.ProbeResult{MGroup: mgroup, Error: nil}
		} else {
			// preallocate map as all those samples will be stored in the map
			// give it 50% more space
			if visitedSamples == nil {
				visitedSamples = make(visitedSampleMap, len(metricFile.Samples)*150/100)
				setSamples(visitedSamples, metricFile.Samples)
			} else if metricFile.Aggregation == AGGREGATE_MIN {
				mergeWithMinSamples(visitedSamples, metricFile.Samples)
			} else if metricFile.Aggregation == AGGREGATE_MAX {
				mergeWithMaxSamples(visitedSamples, metricFile.Samples)
			} else if metricFile.Aggregation == AGGREGATE_SUM {
				mergeWithAppendSamples(visitedSamples, metricFile.Samples)
			} else {
				panic("Unexpected aggregation '" + metricFile.Aggregation + "'")
			}
		}
	}

	if len(visitedSamples) > 0 {
		samples := make(probe.SampleSlice, len(visitedSamples))
		i := 0
		for key, value := range visitedSamples {
			samples[i] = probe.Sample{Key: key, Value: value}
			i += 1
		}

		sort.Sort(samples)

		mgroup := probe.MetricGroup{
			MetricGroupMetadata: probe.MetricGroupMetadata{
				Type: key.typ,
				Agg:  key.agg,
			},
			Samples: samples,
		}
		ch <- probe.ProbeResult{MGroup: mgroup, Error: nil}
	}
}

// Group files into logical groups of metric type and aggregation so that:
// (type1, agg1) -> [matching files]
// (type1, agg2) -> [matching files]
// ...
// (typeN, aggM) -> [matching files]
//
// These groups can be processed in parallel.
func groupFiles(files []string) (map[metricGroupKey][]string, error) {
	fileGroups := make(map[metricGroupKey][]string)
	for _, fpath := range files {
		md, err := metaDataFromPath(fpath)
		if err != nil {
			return nil, err
		}
		key := metricGroupKey{md.MetricType, md.Aggregation}
		group := fileGroups[key]
		group = append(group, fpath)
		fileGroups[key] = group
	}
	return fileGroups, nil
}

func (p *mmapdbProbe) Probe() ([]probe.MetricGroup, error) {
	ch := make(chan probe.ProbeResult)

	go func() {
		defer close(ch)

		if err := p.ProbeAsync(ch); err != nil {
			ch <- probe.ProbeResult{Error: err}
		}
	}()

	var mgroups []probe.MetricGroup
	for res := range ch {
		if res.Error != nil {
			return nil, res.Error
		}
		mgroups = append(mgroups, res.MGroup)
	}

	return mgroups, nil
}

func setSamples(visitedSamples visitedSampleMap, fileSamples probe.SampleSlice) {
	for _, sample := range fileSamples {
		visitedSamples[sample.Key] = sample.Value
	}
}

func mergeWithAppendSamples(visitedSamples visitedSampleMap, fileSamples probe.SampleSlice) {
	for _, sample := range fileSamples {
		visitedSamples[sample.Key] += sample.Value
	}
}

func mergeWithMinSamples(visitedSamples visitedSampleMap, fileSamples probe.SampleSlice) {
	for _, sample := range fileSamples {
		if visitedSample, ok := visitedSamples[sample.Key]; !ok || sample.Value < visitedSample {
			visitedSamples[sample.Key] = sample.Value
		}
	}
}

func mergeWithMaxSamples(visitedSamples visitedSampleMap, fileSamples probe.SampleSlice) {
	for _, sample := range fileSamples {
		if visitedSample, ok := visitedSamples[sample.Key]; !ok || sample.Value > visitedSample {
			visitedSamples[sample.Key] = sample.Value
		}
	}
}
