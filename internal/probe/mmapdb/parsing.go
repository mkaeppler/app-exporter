package mmapdb

import (
	"encoding/binary"
	"errors"
	"io"
	"math"
	"os"
	"path"
	"regexp"

	"gitlab.com/gitlab-org/app-exporter/internal"
	"gitlab.com/gitlab-org/app-exporter/internal/probe"
)

const (
	ENTRY_HEADER_LEN = 4

	METRIC_TYPE_COUNTER   = "counter"
	METRIC_TYPE_GAUGE     = "gauge"
	METRIC_TYPE_HISTOGRAM = "histogram"
	METRIC_TYPE_SUMMARY   = "summary"

	AGGREGATE_ALL = "all"
	AGGREGATE_SUM = "livesum"
	AGGREGATE_MIN = "min"
	AGGREGATE_MAX = "max"

	AVERAGE_SAMPLE_SIZE = 200
)

type FileMetadata struct {
	MetricType  string
	Aggregation string
	Pid         string
}

type MetricFile struct {
	FileMetadata
	ContentLen uint32
	Samples    []probe.Sample
}

var fileRegexp = regexp.MustCompile(`^(counter|gauge|histogram|summary)_(all|livesum|min|max)?_?(\w+)(-\d+)\.db$`)

func ParseFile(filePath string) (*MetricFile, error) {
	fileMetadata, err := metaDataFromPath(filePath)
	if err != nil {
		return nil, err
	}
	internal.Log("MMAP parse", filePath, fileMetadata)

	f, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	// Read first 4 bytes - length of metric data in file.
	var contentLen uint32
	err = binary.Read(f, binary.LittleEndian, &contentLen)
	if err != nil {
		return nil, err
	}

	contentBytes := make([]byte, contentLen)
	if _, err := io.ReadFull(f, contentBytes); err != nil {
		return nil, err
	}

	// discard 4 bytes
	offset := int64(4)
	content := string(contentBytes)
	samples := make([]probe.Sample, 0, contentLen/AVERAGE_SAMPLE_SIZE)

	for offset > 0 && offset < int64(contentLen) {
		nextOffset, sample, err := readNextSample(offset, content, fileMetadata)
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, err
		}

		samples = append(samples, sample)
		offset = nextOffset
	}

	return &MetricFile{*fileMetadata, contentLen, samples[:]}, nil
}

// Read metric type, aggregation and pid from file name.
// For gauges, filenames contain an aggregation function,
// for other metrics it does not:
// - gauge_livesum_my_pid-0.db
// - counter_my_pid-0.db
func metaDataFromPath(filePath string) (*FileMetadata, error) {
	_, fileName := path.Split(filePath)

	matches := fileRegexp.FindAllStringSubmatch(fileName, -1)
	if matches == nil {
		return nil, errors.New("Unexpected file name: " + fileName)
	}

	metricType := matches[0][1]
	aggregation := matches[0][2]
	if aggregation == "" {
		aggregation = AGGREGATE_SUM
	}
	pid := matches[0][3]

	return &FileMetadata{MetricType: metricType, Aggregation: aggregation, Pid: pid}, nil
}

// An entry consists of a 4 byte header containing the entry length plus
// the 8 byte aligned content (metric key and value).
func readNextSample(offset int64, content string, metadata *FileMetadata) (int64, probe.Sample, error) {
	encodedLen := binary.LittleEndian.Uint32([]byte(content[offset : offset+4]))
	if encodedLen == 0 {
		// no more metrics
		return -1, probe.Sample{}, io.EOF
	}
	offset += 4

	if offset+int64(encodedLen) > int64(len(content)) {
		return -1, probe.Sample{}, errors.New("metric content is too long")
	}

	key := content[offset : offset+int64(encodedLen)]
	offset += int64(encodedLen)

	// Metric content is padded to 8 bytes; skip any remaining empty bytes.
	entryLen := ENTRY_HEADER_LEN + encodedLen
	padding := 8 - entryLen%8
	offset += int64(padding)

	if offset+8 > int64(len(content)) {
		return -1, probe.Sample{}, errors.New("metric is missing value")
	}

	value := math.Float64frombits(binary.LittleEndian.Uint64([]byte(content[offset : offset+8])))
	offset += 8

	sample := probe.Sample{
		Key:   key,
		Value: value,
	}

	return offset, sample, nil
}
