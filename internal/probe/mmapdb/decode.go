package mmapdb

import (
	"fmt"
	"strings"

	"gitlab.com/gitlab-org/app-exporter/internal/probe"
)

type DecodedKey struct {
	Metric string // The primary metric name. Ex.: http_request_duration_seconds
	Name   string // A secondary metric name. Ex.: http_request_duration_seconds_bucket
	Type   string // The kind of metric. Ex.: counter
	Labels probe.LabelSlice
}

// The key is encoded as a nested JSON array using the following format:
// [metric_name, child_metric_name, [label_a, label_b, ...], [label_a_value, label_b_value, ...]]
//
// The internal JSON decoder uses reflection, which makes it slow.
// We take advantage of knowing the precise structure of an encoded entry upfront
// and process the string manually instead. Plus, since label values will be
// rendered to strings again anyway, there is no need to first convert them
// to Golang data types.
func Decode(decoded *DecodedKey, sample probe.Sample, mg probe.MetricGroupMetadata) error {
	var err error

	// remove leading '[' and trailing ']]'
	strippedKey := sample.Key[1 : len(sample.Key)-2]

	// This should yield the 3 expected parts (so, two occurrences of `[`): names, labelNames, labelValues
	partsCount := strings.Count(strippedKey, "[")
	if partsCount != 2 {
		return fmt.Errorf("Unexpected key format in key '%s', %d", sample.Key, partsCount)
	}

	metricNames := nextToken(&strippedKey, '[')

	name1 := nextToken(&metricNames, ',')
	name1 = name1[1 : len(name1)-1]
	decoded.Metric = name1

	name2 := nextToken(&metricNames, ',')
	name2 = name2[1 : len(name2)-1]
	decoded.Name = name2

	pidIndex := -1

	labelNameStr := nextToken(&strippedKey, '[')

	decoded.Labels = decoded.Labels[:0]

	if labelNameStr == "]," {
		// empty label array i.e. '[]' token
	} else {
		// drop the trailing '],'
		labelNameStr = labelNameStr[:len(labelNameStr)-2]
		labelValueStr := strippedKey
		pidIndex, err = decodeLabels(&decoded.Labels, labelNameStr, labelValueStr)
		if err != nil {
			return fmt.Errorf("Unexpected key format in key '%s': %w", sample.Key, err)
		}
	}

	// Insert a `pid` label for gauges with aggregation ALL, unless one already exists.
	// This is because they will not be aggregated across files, so must be distinct
	// per process.
	if pidIndex < 0 && mg.Pid != "" && mg.Type == METRIC_TYPE_GAUGE && mg.Agg == AGGREGATE_ALL {
		decoded.Labels = append(decoded.Labels, probe.Label{Name: "pid", Value: mg.Pid})
	}

	return nil
}

func nextToken(value *string, sep byte) string {
	idx := strings.IndexByte(*value, sep)

	if idx > 0 {
		result := (*value)[0:idx]
		*value = (*value)[idx+1:]
		return result
	} else {
		result := *value
		*value = ""
		return result
	}
}

func decodeLabels(labels *probe.LabelSlice, labelNameStr string, labelValueStr string) (int, error) {
	labelNamesCount := strings.Count(labelNameStr, ",") + 1
	labelValueStrCount := strings.Count(labelValueStr, ",") + 1
	if labelNamesCount != labelValueStrCount {
		return -1, fmt.Errorf("Label names and values cardinality mismatch")
	}

	pidIndex := -1
	for i := 0; i < labelNamesCount; i += 1 {
		ln := nextToken(&labelNameStr, ',')

		// Normalize label names.
		ln = ln[1 : len(ln)-1] // strip quotes
		if ln == "pid" {
			pidIndex = i
		}

		lv := nextToken(&labelValueStr, ',')
		if lv[0] == '"' {
			lv = lv[1 : len(lv)-1] // strip quotes
		} else if lv == "null" {
			lv = ""
		}

		*labels = append(*labels, probe.Label{Name: ln, Value: lv})
	}

	return pidIndex, nil
}
