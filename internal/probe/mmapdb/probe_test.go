package mmapdb

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/app-exporter/internal/probe"
	"gitlab.com/gitlab-org/app-exporter/internal/testhelpers"
)

func Test_MergeCounterBySum(t *testing.T) {
	p := NewProbe(testhelpers.FixtureFilePath("mmap"), "counter_*")

	mgroups, err := p.Probe()
	require.NoError(t, err)
	require.Equal(t, 1, len(mgroups))

	actualMG := mgroups[0]
	expectedMG := probe.MetricGroup{
		MetricGroupMetadata: probe.MetricGroupMetadata{
			Type: METRIC_TYPE_COUNTER,
			Agg:  AGGREGATE_SUM,
		},
		Samples: probe.SampleSlice{
			{
				Key:   `["test_counter1","test_counter1",["label1","label2"],["str",10]]`,
				Value: 2,
			},
			{
				Key:   `["test_counter1","test_counter1",["label1","label2"],["str",20]]`,
				Value: 2,
			},
			{
				Key:   `["test_counter2","test_counter2",["label1","label2","label3"],["str",10,0.0]]`,
				Value: 1,
			},
			{
				Key:   `["test_counter2","test_counter2",["label1","label2","label3"],["str",20,1.0121212121212122]]`,
				Value: 1,
			},
			{
				Key:   `["test_counter3","test_counter3",["label1","label2"],["str",10]]`,
				Value: 1,
			},
		},
	}

	require.Equal(t, expectedMG, actualMG)
}

func Test_MergeGaugeByMin(t *testing.T) {
	p := NewProbe(testhelpers.FixtureFilePath("mmap"), "gauge_min_*")

	mgroups, err := p.Probe()
	require.NoError(t, err)
	require.Equal(t, 1, len(mgroups))

	actualMG := mgroups[0]
	expectedMG := probe.MetricGroup{
		MetricGroupMetadata: probe.MetricGroupMetadata{
			Type: METRIC_TYPE_GAUGE,
			Agg:  AGGREGATE_MIN,
		},
		Samples: probe.SampleSlice{
			{
				Key:   `["test_gauge_min","test_gauge_min",["label1","label2"],["str",10]]`,
				Value: 1,
			},
			{
				Key:   `["test_gauge_min","test_gauge_min",["label1","label2"],["str",20]]`,
				Value: 2,
			},
		},
	}

	require.Equal(t, expectedMG, actualMG)
}

func Test_MergeGaugeByMax(t *testing.T) {
	p := NewProbe(testhelpers.FixtureFilePath("mmap"), "gauge_max_*")

	mgroups, err := p.Probe()
	require.NoError(t, err)
	require.Equal(t, 1, len(mgroups))

	actualMG := mgroups[0]
	expectedMG := probe.MetricGroup{
		MetricGroupMetadata: probe.MetricGroupMetadata{
			Type: METRIC_TYPE_GAUGE,
			Agg:  AGGREGATE_MAX,
		},
		Samples: probe.SampleSlice{
			{
				Key:   `["test_gauge_max","test_gauge_max",["label1","label2"],["str",10]]`,
				Value: 5,
			},
			{
				Key:   `["test_gauge_max","test_gauge_max",["label1","label2"],["str",20]]`,
				Value: 10,
			},
		},
	}

	require.Equal(t, expectedMG, actualMG)
}

func Test_MergeGaugeBySum(t *testing.T) {
	p := NewProbe(testhelpers.FixtureFilePath("mmap"), "gauge_livesum_*")

	mgroups, err := p.Probe()
	require.NoError(t, err)
	require.Equal(t, 1, len(mgroups))

	actualMG := mgroups[0]
	expectedMG := probe.MetricGroup{
		MetricGroupMetadata: probe.MetricGroupMetadata{
			Type: METRIC_TYPE_GAUGE,
			Agg:  AGGREGATE_SUM,
		},
		Samples: probe.SampleSlice{
			{
				Key:   `["test_gauge_livesum","test_gauge_livesum",["label1","label2"],["str",10]]`,
				Value: 6,
			},
			{
				Key:   `["test_gauge_livesum","test_gauge_livesum",["label1","label2"],["str",20]]`,
				Value: 12,
			},
		},
	}

	require.Equal(t, expectedMG, actualMG)
}

func Test_DoNotMergeGaugeWithAllAggregation(t *testing.T) {
	p := NewProbe(testhelpers.FixtureFilePath("mmap"), "gauge_all_*")

	mgroups, err := p.Probe()
	require.NoError(t, err)
	require.Equal(t, 2, len(mgroups))

	expectedMG0 := probe.MetricGroup{
		MetricGroupMetadata: probe.MetricGroupMetadata{
			Type: METRIC_TYPE_GAUGE,
			Agg:  AGGREGATE_ALL,
			Pid:  "proc_0",
		},
		Samples: probe.SampleSlice{
			{
				Key:   `["test_gauge_all","test_gauge_all",["label1","label2"],["str",10]]`,
				Value: 1,
			},
			{
				Key:   `["test_gauge_all","test_gauge_all",["label1","label2"],["str",20]]`,
				Value: 2,
			},
		},
	}

	expectedMG1 := probe.MetricGroup{
		MetricGroupMetadata: probe.MetricGroupMetadata{
			Type: METRIC_TYPE_GAUGE,
			Agg:  AGGREGATE_ALL,
			Pid:  "proc_1",
		},
		Samples: probe.SampleSlice{
			{
				Key:   `["test_gauge_all","test_gauge_all",["label1","label2"],["str",10]]`,
				Value: 5,
			},
			{
				Key:   `["test_gauge_all","test_gauge_all",["label1","label2"],["str",20]]`,
				Value: 10,
			},
		},
	}

	require.Equal(t, expectedMG0, mgroups[0])
	require.Equal(t, expectedMG1, mgroups[1])
}
