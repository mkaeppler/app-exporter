package mmapdb

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/app-exporter/internal/probe"
)

func Test_DecodeEmptyMetricKey(t *testing.T) {
	encodedKey := `["name","alt_name",[],[]]`
	expectedKey := DecodedKey{
		Metric: "name",
		Name:   "alt_name",
	}

	var decodedKey DecodedKey
	err := Decode(&decodedKey, probe.Sample{Key: encodedKey}, probe.MetricGroupMetadata{})
	require.NoError(t, err)

	require.Equal(t, expectedKey, decodedKey)
}

func Test_DecodeFullMetricKey(t *testing.T) {
	encodedKey := `["name","alt_name",["a","b","c","d"],["str",1,true,null]]`
	expectedKey := DecodedKey{
		Metric: "name",
		Name:   "alt_name",
		Labels: []probe.Label{
			{Name: "a", Value: "str"},
			{Name: "b", Value: "1"},
			{Name: "c", Value: "true"},
			{Name: "d", Value: ""},
		},
	}

	var decodedKey DecodedKey
	err := Decode(&decodedKey, probe.Sample{Key: encodedKey}, probe.MetricGroupMetadata{})
	require.NoError(t, err)

	require.Equal(t, expectedKey, decodedKey)
}

func Test_DecodeMetricKeyWithPidLabelRecordsPidIndex(t *testing.T) {
	encodedKey := `["name","alt_name",["a","pid"],["str","my_proc"]]`
	expectedKey := DecodedKey{
		Metric: "name",
		Name:   "alt_name",
		Labels: []probe.Label{
			{Name: "a", Value: "str"},
			{Name: "pid", Value: "my_proc"},
		},
	}

	var decodedKey DecodedKey
	err := Decode(&decodedKey, probe.Sample{Key: encodedKey}, probe.MetricGroupMetadata{})
	require.NoError(t, err)

	require.Equal(t, expectedKey, decodedKey)
}
