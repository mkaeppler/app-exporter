package main

import (
	"log"
	"os"

	"gitlab.com/gitlab-org/app-exporter/internal/probe"
	"gitlab.com/gitlab-org/app-exporter/internal/probe/mmapdb"
	"gitlab.com/gitlab-org/app-exporter/internal/render"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatalf("Must provide a target directory: %s <path>", os.Args[0])
	}

	targetDir := os.Args[1]
	p := mmapdb.NewDefaultProbe(targetDir)
	ch := make(chan probe.ProbeResult)

	go func() {
		defer close(ch)

		if err := p.ProbeAsync(ch); err != nil {
			log.Fatal(err)
		}
	}()

	if err := render.TextAsync(ch, os.Stdout); err != nil {
		log.Fatal(err)
	}
}
