package main

import (
	"log"
	"net/http"
	"os"

	"gitlab.com/gitlab-org/app-exporter/internal/probe"
	"gitlab.com/gitlab-org/app-exporter/internal/probe/mmapdb"
	"gitlab.com/gitlab-org/app-exporter/internal/render"

	_ "net/http/pprof"
)

func main() {
	port, ok := os.LookupEnv("EXPORTER_PORT")
	if !ok {
		port = "8082"
	}
	metricsDir, ok := os.LookupEnv("EXPORTER_METRICS_DIR")
	if !ok {
		if len(os.Args) < 2 {
			log.Fatalf("Must provide a target directory: %s <path> or use EXPORTER_METRICS_DIR", os.Args[0])
		}
		metricsDir = os.Args[1]
	}

	log.Println("Serving /metrics on", port)
	http.HandleFunc("/metrics", func(rw http.ResponseWriter, r *http.Request) {
		p := mmapdb.NewDefaultProbe(metricsDir)
		ch := make(chan probe.ProbeResult)

		go func() {
			defer close(ch)

			if err := p.ProbeAsync(ch); err != nil {
				log.Println(err)
			}
		}()

		if err := render.TextAsync(ch, os.Stdout); err != nil {
			log.Println(err)
		}
	})
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
