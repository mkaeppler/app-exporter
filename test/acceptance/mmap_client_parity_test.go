package paritytest

import (
	"bytes"
	"os/exec"
	"sort"
	"strings"
	"testing"

	dto "github.com/prometheus/client_model/go"

	"github.com/prometheus/common/expfmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/app-exporter/internal/probe/mmapdb"
	"gitlab.com/gitlab-org/app-exporter/internal/render"
	"gitlab.com/gitlab-org/app-exporter/internal/testhelpers"
)

func Test_Acceptance_SameCounterOutput(t *testing.T) {
	filename := "counter_proc_0-0.db"
	expected := mmapClientSerializeFile(t, filename)
	actual := exporterSerializeFile(t, filename)

	require.Equal(t, expected, actual)
}

func Test_Acceptance_SameGaugeAggregateAllOutput(t *testing.T) {
	filename := "gauge_all_proc_0-0.db"
	expected := mmapClientSerializeFile(t, filename)
	actual := exporterSerializeFile(t, filename)

	require.Equal(t, expected, actual)
}

func Test_Acceptance_SameHistogramOutput(t *testing.T) {
	filename := "histogram_proc_0-0.db"
	expected := mmapClientSerializeFile(t, filename)
	actual := exporterSerializeFile(t, filename)

	require.Equal(t, expected, actual)
}

func Test_Acceptance_SameDirOutput(t *testing.T) {
	expected := mmapClientSerializeAll(t, "mmap")
	actual := exporterSerializeAllAsync(t, "mmap")

	requireMetricFamiliesMatch(t, expected, actual)
}

func Test_Acceptance_SameDirOutputWithRealWorldData(t *testing.T) {
	expected := mmapClientSerializeAll(t, "mmap/realworld")
	actual := exporterSerializeAllAsync(t, "mmap/realworld")

	requireMetricFamiliesMatch(t, expected, actual)
}

func decodeOutput(t *testing.T, output string) map[string]*dto.MetricFamily {
	// sort metrics to ensure the same order of all with labels
	lines := strings.Split(output, "\n")
	sort.Strings(lines)
	output = strings.Join(lines, "\n") + "\n"

	parser := expfmt.TextParser{}
	res, err := parser.TextToMetricFamilies(strings.NewReader(output))
	require.NoError(t, err)
	return res
}

func mmapClientSerializeFile(t *testing.T, filename string) map[string]*dto.MetricFamily {
	return decodeOutput(
		t, runMmapClient(t, "../../ruby-client-mmap/serialize_file.rb", "../../test/data/mmap/"+filename),
	)
}

func mmapClientSerializeAll(t *testing.T, dir string) map[string]*dto.MetricFamily {
	return decodeOutput(
		t, runMmapClient(t, "../../ruby-client-mmap/serialize_dir.rb", "../../test/data/"+dir),
	)
}

func runMmapClient(t *testing.T, scriptPath string, args ...string) string {
	cmd := exec.Command(scriptPath, args...)

	var out bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &out
	err := cmd.Run()
	require.NoError(t, err, out.String())

	return out.String()
}

func exporterSerializeFile(t *testing.T, filename string) map[string]*dto.MetricFamily {
	output, err := render.MmapFilesText(testhelpers.FixtureFilePath("mmap"), filename)
	require.NoError(t, err)

	return decodeOutput(t, output)
}

func exporterSerializeAll(t *testing.T, dir string) map[string]*dto.MetricFamily {
	p := mmapdb.NewDefaultProbe(testhelpers.FixtureFilePath(dir))
	metrics, err := p.Probe()
	require.NoError(t, err)

	var buf bytes.Buffer
	err = render.Text(metrics, &buf)
	require.NoError(t, err)

	return decodeOutput(t, buf.String())
}

func exporterSerializeAllAsync(t *testing.T, dir string) map[string]*dto.MetricFamily {
	p := mmapdb.NewDefaultProbe(testhelpers.FixtureFilePath(dir))

	mgroups, err := p.Probe()
	require.NoError(t, err)

	var buf bytes.Buffer
	err = render.Text(mgroups, &buf)
	require.NoError(t, err)

	return decodeOutput(t, buf.String())
}

func findMatchingMetric(t *testing.T, metrics []*dto.Metric, labels []*dto.LabelPair) *dto.Metric {
	for _, metric := range metrics {
		if len(metric.Label) != len(labels) {
			continue
		}

		for _, label := range labels {
			for _, metricLabel := range metric.Label {
				if assert.ObjectsAreEqual(label, metricLabel) {
					goto foundLabel
				}
			}

			goto missingLabel

		foundLabel:
		}

		return metric

	missingLabel:
	}

	return nil
}

func requireMetricFamiliesMatch(t *testing.T, expected, actual map[string]*dto.MetricFamily) {
	requireSameKeys(t, expected, actual)

	for k := range expected {
		expectedMF := expected[k]
		actualMF := actual[k]
		require.Equal(t, expectedMF.Name, actualMF.Name)
		require.Equal(t, expectedMF.Help, actualMF.Help)
		require.Equal(t, expectedMF.Type, actualMF.Type)
		require.Equal(t, len(expectedMF.Metric), len(actualMF.Metric))

		for _, m1 := range expectedMF.Metric {
			m2 := findMatchingMetric(t, actualMF.Metric, m1.Label)

			require.NotNil(t, m2, "Labels not found for", *expectedMF.Name, m1.Label)
			require.Equal(t, m1.Counter, m2.Counter)
			require.Equal(t, m1.Gauge, m2.Gauge)
			require.Equal(t, m1.Histogram, m2.Histogram)
			require.Equal(t, m1.Summary, m2.Summary)
			require.Equal(t, m1.Untyped, m2.Untyped)
			require.ElementsMatch(t, m1.Label, m2.Label)
		}
	}
}

func requireSameKeys(t *testing.T, m1, m2 map[string]*dto.MetricFamily) {
	keys1 := make([]string, len(m1))
	for k := range m1 {
		keys1 = append(keys1, k)
	}
	keys2 := make([]string, len(m2))
	for k := range m2 {
		keys2 = append(keys2, k)
	}
	require.ElementsMatch(t, keys1, keys2)
}
