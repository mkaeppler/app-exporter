package benchmark

import (
	"io/fs"
	"io/ioutil"
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/app-exporter/internal/probe/mmapdb"
	"gitlab.com/gitlab-org/app-exporter/internal/render"
	"gitlab.com/gitlab-org/app-exporter/internal/testhelpers"
)

func Benchmark_Stage1_ParseAllFiles(b *testing.B) {
	b.ReportAllocs()

	dirPath := testhelpers.FixtureFilePath("mmap/realworld")
	fileSystem := os.DirFS(dirPath)
	files, err := fs.Glob(fileSystem, "*.db")
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		for _, file := range files {
			filePath := path.Join(dirPath, file)
			metricFile, err := mmapdb.ParseFile(filePath)
			require.NoError(b, err)
			require.NotEmpty(b, metricFile.Samples)
		}
	}
}

func Benchmark_Stage2_MmapProbeAll(b *testing.B) {
	b.ReportAllocs()

	probe := mmapdb.NewDefaultProbe(testhelpers.FixtureFilePath("mmap/realworld"))
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		samples, err := probe.Probe()
		require.NoError(b, err)
		require.NotEmpty(b, samples)
	}
}

func Benchmark_Stage3_MmapRenderAll(b *testing.B) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		output, err := render.MmapFilesText(testhelpers.FixtureFilePath("mmap/realworld"), "*.db")
		require.NoError(b, err)
		require.NotEmpty(b, output)
	}
}

func Benchmark_ParseCounterFile(b *testing.B) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		f, err := mmapdb.ParseFile(testhelpers.FixtureFilePath("mmap/realworld/counter_puma_0-0.db"))
		require.NoError(b, err)
		require.NotEmpty(b, f.Samples)
	}
}

func Benchmark_ParseHistogramFile(b *testing.B) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		f, err := mmapdb.ParseFile(testhelpers.FixtureFilePath("mmap/realworld/histogram_puma_0-0.db"))
		require.NoError(b, err)
		require.NotEmpty(b, f.Samples)
	}
}

func Benchmark_RenderMmapText(b *testing.B) {
	b.ReportAllocs()

	samples, err := mmapdb.NewDefaultProbe(testhelpers.FixtureFilePath("mmap/realworld")).Probe()
	require.NoError(b, err)
	require.NotEmpty(b, samples)

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		err = render.Text(samples, ioutil.Discard)
		require.NoError(b, err)
	}
}
