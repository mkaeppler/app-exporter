# GitLab Metrics Exporter

**NOTE**: This project is under development and considered experimental. It is not in use at GitLab currently.

---

GitLab Metrics Exporter is meant to subsume the functionality of [gitlab-exporter](https://gitlab.com/gitlab-org/gitlab-exporter/)
and in-app Ruby Prometheus exporters distributed with and running in [gitlab-rails](https://gitlab.com/gitlab-org/gitlab).

It has the following goals:

- Provide a single, unified mechanism to export application-specific metrics to Prometheus.
- Ability to run as a standalone system or as a sidecar process to gitlab-rails.
- Ability to collect and serve a variety of metrics using a flexible approach to probing.
- Ability to collect and serve metrics in a memory- and CPU-efficient way.
- Ability to exploit concurrency when running multiple probes.

## Architecture

GitLab Metrics Exporter follows a simple design based on two primary concepts:

- **Probes.** A probe is responsible for sampling data. It is up to the probe how that is accomplished; it could
  query a connected data store, send HTTP requests, perform IPC or read from the file system. A probe
  returns data in a data structure that can be consumed by a renderer. There can be many probes active in parallel.
- **Renderers.** A renderer takes data returned from one or more probes and serializes it, so it can be ingested by Prometheus.

The exporter exposes a single endpoint, `/metrics`. When a client requests this endpoint, all probes
will execute in parallel and their results are combined, serialized, and rendered to the client.
This can be understood as a function: `render(merge(probe1(), ..., probeN()))`.

Which probes should run in response to a request is specified in the configuration file.

## Configuration

TODO

## Probes

The following probes are currently implemented:

- `mmapdb`. This probe reads `db` files written by [prometheus-client-mmap](https://gitlab.com/gitlab-org/prometheus-client-mmap). It is the
  format used to emit application metrics from [gitlab-rails](https://gitlab.com/gitlab-org/gitlab).

## Renderers

The following renderers are currently implemented:

- `text`. This renderer implements Prometheus' default [Text Exposition Format](https://prometheus.io/docs/instrumenting/exposition_formats/#text-based-format).
