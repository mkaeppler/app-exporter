BUILD_DIR := bin
GOBUILD := go build

PKG := gitlab.com/gitlab-org/app-exporter

.PHONY: all
all: clean cli srv

.PHONY: cli
cli:
	$(GOBUILD) -o $(BUILD_DIR)/exporter-cli $(PKG)/cmd/$@

.PHONY: srv
srv:
	$(GOBUILD) -o $(BUILD_DIR)/exporter-srv $(PKG)/cmd/$@

.PHONY: lint
lint:
	go fmt ./...
	go vet ./...

.PHONY: test
test: test-unit test-acceptance

.PHONY: test-unit
test-unit:
	go test -v ./internal/...

.PHONY: test-acceptance
test-acceptance:
	bundle install --gemfile=ruby-client-mmap/Gemfile
	go test -v ./test/acceptance -run Test_Acceptance

.PHONY: verify
verify: lint test

.PHONY: bench
bench:
	go test ./test/benchmark -bench=.

.PHONY: profile
profile:
	go test -cpuprofile cpu.prof -memprofile mem.prof ./test/benchmark -bench=.

.PHONY: clean
clean:
	rm -f bin/*
